import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerServiceService } from '../customer-service.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
 
  
  
    
    message:any;
  
   
    constructor(private service: CustomerServiceService,private router:Router){
     
    }
    
    submitRegForm(regForm: any){
      console.log(regForm);
  
      if (regForm.password == regForm.repassword) {
      this.service.register(regForm).subscribe();
      alert("successfully registered");
      this.router.navigate(['login']);
   } else {
      alert("Enter your details correctly");
    }
  
  
    }
  }

