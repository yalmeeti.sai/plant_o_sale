import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './home/header/header.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { authGuard } from './auth.guard';
import { RegisterProductComponent } from './register-product/register-product.component';


const routes: Routes = [
  {path:'',component:HomeComponent,pathMatch:'full'},
   {path:'login',component:LoginComponent},
  // {path:'employee',canActivate:[authGuard],component:ShowEmployeesComponent},
  // {path:'showproducts',canActivate:[authGuard],component:ProductsComponent},
  {path:'register',component:RegisterComponent},
  {path:'home',component:HomeComponent},
  { path: 'registerProduct', component: RegisterProductComponent },
  {path:'header',component:HeaderComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
