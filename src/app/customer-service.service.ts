import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerServiceService {
  cartItems: any;

  
  constructor(private http: HttpClient) { 
    this.cartItems = [];
  }

  getCustomers() {
    return this.http.get('getAllCustomers');
  }
  
  register(regForm: any) {
    return this.http.post('registerCustomer', regForm);
  }


  registerProduct(productForm: any) {
    return this.http.post('registerProduct', productForm);
  }

  getAllProducts(){
    return this.http.get('getProducts');
  }

  addToCart(customerId: number, productId: number): Observable<number> {
    const url = /addToCart/${customerId}/${productId};
    return this.http.post<number>(url, null); // Send a POST request with an empty body
  }


  getUserCart(customerId: number): Observable<any > {
    // Use a relative URL that matches the proxy configuration
    const url = /getCustomerCart/${customerId};
    return this.http.get<any[]>(url);
  }


  deleteCustomerCartProduct(customerId: number, productId: number): Observable<any> {
    const url = deleteCustomerCartProduct/${customerId}/${productId};
    return this.http.delete<any>(url);
  }


  deleteCustomerCartSingleProduct(customerId: any, productId: any): Observable<any> {
    const url = /deleteCustomerCartSingleProduct/${customerId}/${productId};
    return this.http.delete<any>(url);
  }

  deleteCustomerCart(customerId: any):Observable<any> {
    const url = /deleteCustomerCart/${customerId};
    return this.http.delete<any>(url);
  }

  
  deleteCartById(cartId: any):Observable<any> {
    const url = /deleteCartById/${cartId};
    return this.http.delete<any>(url);
  }


  registerOrder(FormData: any) {
    return this.http.post('addOrders', FormData);
  }


  getAllOrders() {
    return this.http.get('getAllOrders');
  }

  getCustomerOrder(customerId: number): Observable<any > {
    // Use a relative URL that matches the proxy configuration
    const url = /getCustomerOrder/${customerId};
    return this.http.get<any[]>(url);
  }
  

 

  

}
