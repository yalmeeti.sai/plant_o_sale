import { Component,OnInit } from '@angular/core';

import { CustomerServiceService } from '../customer-service.service';

@Component({
  selector: 'app-show-product',
  templateUrl: './show-product.component.html',
  styleUrls: ['./show-product.component.css']
})
export class ShowProductComponent implements OnInit {


  products:any;
  ngOnInit(): void {}
  constructor(private service : CustomerServiceService){
    this.service.getAllProducts().subscribe((data : any)=>{
      this.products =data;
      this.products.forEach((product:any) => {
        product.imageUrl = 'data:image/jpeg;base64,' + product.imageData; // Assuming it's a JPEG image
      });
      console.log(this.products)
    })
  }


}
