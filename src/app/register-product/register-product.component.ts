import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CustomerServiceService } from '../customer-service.service';

@Component({
  selector: 'app-register-product',
  templateUrl: './register-product.component.html',
  styleUrls: ['./register-product.component.css']
})
export class RegisterProductComponent implements OnInit{
  products: any;
  product = {
    prodCategory: '',
    prodTitle: '',
    prodCost:0,
    prodDesc: '',
    file:''
  };
 


  constructor(private service:CustomerServiceService,private router:Router) { }
 
    ngOnInit(): void {
    }
   

  submitForm() {
    const formData = new FormData();
    formData.append('prodCategory', this.product.prodCategory);
    formData.append('prodTitle', this.product.prodTitle);
    formData.append('prodCost', this.product.prodCost.toString());
    formData.append('prodDesc', this.product.prodDesc);
    formData.append('file', this.product.file);

    this.service.registerProduct(formData).subscribe(

    (response:any) => {
      // Handle the response from your Spring Boot backend here
      console.log(response);
    },
    (error:any) => {
      // Handle any errors that occur during the HTTP request
      console.error(error);
    }
  );
  alert("product added successfully");
  this.router.navigate(['showProducts']);
}

onFileSelected(event: any) {
  const files = event.target.files;
  if (files.length > 0) {
    this.product.file = files[0];
  }
}



}