import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  contactForm = {
    name: '',
    email: '',
    message: ''
  };

  constructor(private http: HttpClient) {} 

  submitContactForm() {
    
    const formData = {
      name: this.contactForm.name,
      email: this.contactForm.email,
      message: this.contactForm.message
    };

   
    this.http.post('/api/contact/submit', formData)
      .subscribe(
        (response) => {
          console.log('Contact form submitted successfully:', response);
                  this.resetForm();
        },
        (error) => {
          console.error('Contact form submission failed:', error);
                }
      );
  }

  resetForm() {
    
    this.contactForm = {
      name: '',
      email: '',
      message: ''
    };
  }
}