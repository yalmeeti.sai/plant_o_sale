import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// import { MatSnackBarModule } from '@angular/material/snack-bar'; // Import MatSnackBar for displaying alerts
import { FormsModule } from '@angular/forms';
import{  HttpClientModule} from '@angular/common/http'
import { SocialLoginModule, SocialAuthServiceConfig } from '@abacritt/angularx-social-login';

import {GoogleLoginProvider,GoogleSigninButtonModule} from '@abacritt/angularx-social-login';

import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './home/header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './home/footer/footer.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { RegisterProductComponent } from './register-product/register-product.component';
import { ShowProductComponent } from './show-product/show-product.component';
import { CartComponent } from './cart/cart.component';
import { PaymentComponent } from './payment/payment.component';
import { ContactComponent } from './contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    RegisterProductComponent,
    ShowProductComponent,
    CartComponent,
    PaymentComponent,
    ContactComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    // MatSnackBarModule,
    RouterModule,
    SocialLoginModule,
    GoogleSigninButtonModule,
    ToastrModule.forRoot() // ToastrModule added here


    
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '581136523940-49jdvhsrtoeqjaf8spu88n9k139bf5f8.apps.googleusercontent.com'
            )
          }
        
        ],
        onError: (err) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
