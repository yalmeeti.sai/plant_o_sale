import { CustomerServiceService } from '../customer-service.service';
import { Router } from '@angular/router';

    import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
 import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {

  customerId: any;
  cartItems: any;
  totalPrice: any = 0;
  totalItem : number = 0;

  constructor(private router: Router, private service: CustomerServiceService,private toastr:ToastrService) { }


  ngOnInit(): void {
    this.getCustomerCart();
    
  }

  getCustomerCart() {
    this.customerId = localStorage.getItem('userId');
    this.service.getUserCart(this.customerId)
      .subscribe((data: any) => {
        this.cartItems = data;
        this.totalItem = data.length;
        console.log(this.cartItems);
        this.cartItems.forEach((product: any) => {
          product.imageUrl = 'data:image/jpeg;base64,' + product.imagedata;
        });
        this.calculateTotalPrice();
      });
  }



  calculateTotalPrice() {
    this.totalPrice = 0;
    this.cartItems.forEach((item: { price: any; }) => {
      this.totalPrice += item.price;
    });
  }

  deleteProductFromCart(productId: any) {
    this.service.deleteCustomerCartProduct(this.customerId, productId)
      .subscribe((response: any) => {
        if (response.success) {
          this.cartItems = this.cartItems.filter((item: { productId: any; }) => item.productId !== productId);
          this.calculateTotalPrice();
          this.getCustomerCart();
          
        } else {
          console.log('Failed to remove item from cart.');
        }
      });
  }

  deleteSingleProductFromCart(productId: any) {
    this.service.deleteCustomerCartSingleProduct(this.customerId, productId)
      .subscribe((response:any)=>{
        console.log(response);
        this.calculateTotalPrice();
               
      },
      err=>{console.log(err);
      }
      );
      this.toastr.success("cart item deleted");
      this.getCustomerCart();
  }



  deleteCart() {
    this.service.deleteCustomerCart(this.customerId)
      .subscribe((response: any) => {
        if (response.success) {
          this.cartItems = [];
          this.calculateTotalPrice();
         
        } else {
          console.log('Failed to clear cart.');
        }
      });
      this.getCustomerCart();
  }



  placeOrder(){
    this.router.navigate(['payment/',this.totalPrice]);
  }



}