package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Plant_Products;
@Repository
public interface ProductsRepository extends JpaRepository<Plant_Products,Long>{

}
