
package com.dao;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.model.Plant_Products;

@Service
public class ProductsDao {
	
	@Autowired
	private ProductsRepository prodRepo;


	public String savePlant_Products(String prodCategory,String prodTitle,double prodCost,String prodDesc,MultipartFile file ) throws IOException {
		Plant_Products image = new Plant_Products();
		image.setProdCategory(prodCategory);
		image.setProdTitle(prodTitle);
		image.setProdCost(prodCost);
		image.setProdDesc(prodDesc);
		image.setImageData(file.getBytes());
		prodRepo.save(image);
		return "Image saved in DB";
	}
	

	

	
	
	public List<Plant_Products> getAllProducts() {
		return prodRepo.findAll();
	}     
	
	
//	public Optional<Products> getProductById(int id){
//	Product cust =new Product(1,"raghu","male","9876543210","raghu@ts.com","raghu123");
	 //  return prodRepo.findById(id);
//		return prodRepo.findById((id);	
	//}
//	
//	public Plant_Products registerProduct(Plant_Products products, MultipartFile image) throws IOException {
//		 products.setImagedata(image.getBytes());
//		return prodRepo.save(products);
//	}
	
//	public Products updateProduct(Products products){
//		return prodRepo.save(products);
//	}
//	public void deleteProductById(int id){
//	 prodRepo.deleteById(id);
//	 
//	}
//	
//	

}