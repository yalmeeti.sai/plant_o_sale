package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Customer;



@Service
public class CustomerDao {
	
	@Autowired
	CustomerRepository ecomRepo;
	
	public List<Customer> getAllCustomers() {
		return ecomRepo.findAll();
	}     
	
	public Customer getCustomerById(int id){
	Customer cust =new Customer(1,"raghu","male","9876543210","raghu@ts.com","raghu123");
	   return ecomRepo.findById(id).orElse(cust);
//		return ecomRepo.findById((id);
		
	}
	public Customer registerCustomer(Customer Customer) {
		return ecomRepo.save(Customer);
	}
	
	public Customer updateCustomer(Customer Customer){
		return ecomRepo.save(Customer);
	}
	public void deleteCustomerById(int id){
	 ecomRepo.deleteById(id);
	 
	}
	

}