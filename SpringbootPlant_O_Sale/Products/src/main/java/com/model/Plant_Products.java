package com.model;

import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity 
public class Plant_Products {
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
  private  int id;
  private String prodCategory;
  private String prodTitle;
  private double prodCost;
  private String prodDesc;
  
@Lob
  private byte[] imageData;

public Plant_Products(){}


public Plant_Products(int id, String prodCategory, String prodTitle, double prodCost, String prodDesc,
		byte[] imageData) {
	super();
	this.id = id;
	this.prodCategory = prodCategory;
	this.prodTitle = prodTitle;
	this.prodCost = prodCost;
	this.prodDesc = prodDesc;
	this.imageData = imageData;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getProdCategory() {
	return prodCategory;
}

public void setProdCategory(String prodCategory) {
	this.prodCategory = prodCategory;
}

public String getProdTitle() {
	return prodTitle;
}

public void setProdTitle(String prodTitle) {
	this.prodTitle = prodTitle;
}

public double getProdCost() {
	return prodCost;
}

public void setProdCost(double prodCost) {
	this.prodCost = prodCost;
}

public String getProdDesc() {
	return prodDesc;
}

public void setProdDesc(String prodDesc) {
	this.prodDesc = prodDesc;
}

public byte[] getImageData() {
	return imageData;
}

public void setImageData(byte[] imageData) {
	this.imageData = imageData;
}

@Override
public String toString() {
	return "Plant_Products [id=" + id + ", prodCategory=" + prodCategory + ", prodTitle=" + prodTitle + ", prodCost="
			+ prodCost + ", prodDesc=" + prodDesc + ", imageData=" + Arrays.toString(imageData) + "]";
}

}
