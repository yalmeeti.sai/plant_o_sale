package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
  private  int id;
  private String CusName;
  private String Gender;
  private String Mobile;
  private String email;
  private String password;
  
  public Customer(){}
public Customer(int id, String cusName, String gender, String mobile, String email, String password) {
	super();
	this.id = id;
	CusName = cusName;
	Gender = gender;
	Mobile = mobile;
	this.email = email;
	this.password = password;
}


public int getId() {
	return id;
}


public void setId(int id) {
	this.id = id;
}


public String getCusName() {
	return CusName;
}


public void setCusName(String cusName) {
	CusName = cusName;
}


public String getGender() {
	return Gender;
}


public void setGender(String gender) {
	Gender = gender;
}


public String getMobile() {
	return Mobile;
}


public void setMobile(String mobile) {
	Mobile = mobile;
}


public String getEmail() {
	return email;
}


public void setEmail(String email) {
	this.email = email;
}


public String getPassword() {
	return password;
}


public void setPassword(String password) {
	this.password = password;
}


@Override
public String toString() {
	return "Customer [id=" + id + ", CusName=" + CusName + ", Gender=" + Gender + ", Mobile=" + Mobile + ", email="
			+ email + ", password=" + password + "]";
}
  


  
}
