
	package com.model;

	import java.util.Optional;

	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
	import javax.persistence.JoinColumn;
	import javax.persistence.ManyToMany;
	import javax.persistence.ManyToOne;
	import javax.persistence.OneToOne;

	@Entity
	public class Cart {
		 @Id
		    @GeneratedValue(strategy = GenerationType.IDENTITY)
		    private int cartId;
		    
		    @ManyToOne
		    @JoinColumn(name = "productId")
		    private Plant_Products product;
		    
		    @ManyToOne
		    @JoinColumn(name = "customerId")
		    private Customer customer;

		    public Cart(){}
		    
			public Cart(int cartId, Plant_Products product, Customer customer) {
				super();
				this.cartId = cartId;
				this.product = product;
				this.customer = customer;
			}

			public int getCartId() {
				return cartId;
			}

			public void setCartId(int cartId) {
				this.cartId = cartId;
			}

			public Plant_Products getProduct() {
				return product;
			}

			public void setProduct(Plant_Products product) {
				this.product = product;
			}

			public Customer getCustomer() {
				return customer;
			}

			public void setCustomer(Customer customer) {
				this.customer = customer;
			}

			@Override
			public String toString() {
				return "CartItem [cartId=" + cartId + ", product=" + product + ", customer=" + customer + "]";
			}
		    

		    
	}


