package com.ts.Products;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dao.ProductsDao;
import com.model.Plant_Products;

@RestController
public class ProductsController {
	@Autowired
	 private ProductsDao prodDao;
	
	@PostMapping("/registerProduct")
	public String saveIMageOrFile(
			@RequestParam("prodCategory") String prodCategory,
			@RequestParam("prodTitle") String prodTitle,
			@RequestParam("prodCost") double prodCost,
			@RequestParam("prodDesc") String prodDesc ,
	        @RequestParam("file") MultipartFile file) throws IOException {
		return prodDao.savePlant_Products(prodCategory,prodTitle,prodCost,prodDesc,file );
	}
	
//    @PostMapping("/registerProduct")
//    public String imageOrFile(
//   		 @RequestParam("prodName") String prodName,
//   		 @RequestParam("prodBrand") String prodBrand,
//   		 @RequestParam("prodDesc") String prodDesc,
//   		 @RequestParam("price") double price,
//   		 @RequestParam("file") MultipartFile file
//   		 ) throws IOException {
//            return prodDao.registerProduct(prodName,prodBrand,prodDesc,price,file);
//        
//    }

     
     @RequestMapping("getProducts")
  	public List<Plant_Products> getAllProducts() {
  		return prodDao.getAllProducts();
  	}
//     
//     @RequestMapping("getProductById/{ID}")
//  	public Optional<Products>  getProductById(@PathVariable("ID")int id){
//  		return prodDao.getProductById(id);
//  	}
	
//     @PostMapping("registerProduct")
// 	public Plant_Products registerProduct(@RequestBody Plant_Products Product) {
// 		return prodDao.registerProduct(Product);
// 	}
   
//     @PostMapping("/add")
//     public ResponseEntity<Products> registerProduct(@RequestBody Products product,MultipartFile image)  throws IOException {
//         Products user=prodDao.registerProduct(product,image);
//         return new ResponseEntity<Products>(user,HttpStatus.OK);
//     }
     
     
//     @PostMapping("/upload")
//     public Product createProduct(@ModelAttribute Product product, @RequestParam("imageFile") MultipartFile imageFile) throws IOException {
//         return prodDao.saveProduct(product, imageFile);
//     }
     
     
     
//     @PostMapping("registerProduct")
//     public ResponseEntity<Products> uploadImage(@RequestParam("file") MultipartFile file) throws IOException {
//         Products savedImage = prodDao.saveImage(file);
//         return ResponseEntity.ok(savedImage);
//     }

     
     
//     @PostMapping("registerProduct")
//     public ResponseEntity<String> uploadProduct(
//             @ModelAttribute Product product,
//             @RequestParam("image") MultipartFile image) {
//         try {
//             prodDao.registerProduct(product, image);
//             return ResponseEntity.ok("Product uploaded successfully.");
//         } catch (IOException e) {
//             return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
//                 .body("Failed to upload product.");
//         }
//     }
// 	
// 	@PutMapping("updateProduct")
// 	public Products updateProducts(@RequestBody Products products){
// 		return prodDao.updateProduct(products);
// 	}
// 	
// 	
// 	@DeleteMapping("deleteProduct")
// 	public void deleteProductById(@RequestParam int id){
// 		prodDao.deleteProductById(id);
// 		
// 	}
 	
 	

}


